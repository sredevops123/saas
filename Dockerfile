FROM ubuntu:latest

WORKDIR /app

RUN apt-get update && \
    apt-get install -y apt-transport-https gnupg2 curl && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

RUN curl -LO https://get.helm.sh/helm-v3.7.0-linux-amd64.tar.gz && \
    tar -zxvf helm-v3.7.0-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf helm-v3.7.0-linux-amd64.tar.gz linux-amd64

RUN curl -sL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get install -y nodejs

COPY ./src ./src
COPY ./helm-charts ./helm-charts
COPY ./patches ./patches
COPY package*.json .

RUN npm ci

EXPOSE 3000

CMD [ "node", "src/index.js" ]