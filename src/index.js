import { promisify } from 'util'
import express from 'express'
import { Helm } from 'node-helm'
import bodyParser from 'body-parser';
import z from 'zod'

const helmBinary = '/usr/local/bin/helm';

const helm = new Helm({ helmCommand: helmBinary })
helm.install = promisify(helm.install)
helm.list = promisify(helm.list)
helm.status = promisify(helm.status)

const app = express()

app.use(bodyParser.json())

app.get('/status', (req, res) => {
    try {
        const statusSchema = z.object({
            releaseName: z.string()
        })

        const options = statusSchema.parse(req.query)
        helm.status(options)
            .then(status => {
                res.send({
                    status
                })
            })
            .catch(error => {
                res.send({
                    error
                })
            })
    } catch (e) {
        res.sendStatus(500)
    }
})

app.post('/install', (req, res) => {
    try {
        const installationSchema = z.object({
            chartName: z.string(),
            releaseName: z.string(),
            namespace: z.string(),
            values: z.record(z.string(), z.any())
        });

        const options = installationSchema.parse(req.body)

        helm.install(options)
            .then((installation) => {
                res.send({
                    options,
                    installation
                })
            })
            .catch(error => {
                res.send({
                    error
                })
            })
    } catch (e) {
        res.sendStatus(500)
    }
})

app.listen(3000, () => console.log('Application is running at: localhost:3000'))